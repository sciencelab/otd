Physics professor [Robert McNees](http://jacobi.luc.edu/) regularly posts [On-This-Day science-related tweet threads](https://twitter.com/search?q=from%3Amcnees%20otd&src=typd) on Twitter.

This app, available at https://sciencelab.gitlab.io/otd, is a searchable collection of these tweet threads along with supplemental information, such as links to biographical information about the person, keyword hashtags, and links to other material related to the OTD event.  A few entries from other OTD science tweeters besides McNees are also included.

This simple app is written solely in HTML and Javascript, with a little CSS. There is no database server backend, just a text file containing the data. Thus this works as a static webpage. The files can be downloaded and run locally on a computer without need for a web server.  Anyone who may wish to extend this app is invited to do so by either downloading the files or forking the project and applying their own customizations.



